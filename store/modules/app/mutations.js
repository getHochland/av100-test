export default {
  SET_DEVICE(state, device) {
    state.device = device;
  },
  SET_USER(state, user){
    state.user = user;
  },
  SET_USER_SETTINGS(state, userSettings){
    state.userSettings = userSettings;
  },
  SET_TOKEN(state, token){
    state.token = token;
  }
}
