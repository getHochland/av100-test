export default {
  async login({state, commit}, ctx) {
    const config = {
      headers: {
        'X-Api-Key': '8bcfb6e1-4fa8-4fae-872c-a435bbdbe8d9',
      }
    }
    if (ctx.requestType === 'client') {
      // Если запрос с клиента, то мы можем получить os
      config.headers['X-Device-OS'] = state.device.os.name || null;
    }
    return await this.$axios.post(`/login`, ctx.body, config)
  },
  async changeUserEmail({state}, email) {
    const config = {
      headers: {
        'X-Api-Key': '8bcfb6e1-4fa8-4fae-872c-a435bbdbe8d9',
        'X-Device-OS': state.device.os.name || null,
        'X-User-Token': state.token
      }
    }
    return await this.$axios.put(`/user/${state.user.id}`, {email: email}, config)
  },
  async getUserSettings({state}) {
    const config = {
      headers: {
        'X-Api-Key': '8bcfb6e1-4fa8-4fae-872c-a435bbdbe8d9',
        'X-User-Token': state.token
      }
    }
    return await this.$axios.get(`user`, config)
  }
}
