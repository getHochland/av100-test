import appModule from './modules/app';

const createStore = {
  namespaced: true,
  modules: {
    app: appModule,
  }
};

export default createStore
